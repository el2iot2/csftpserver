﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace csftpserver
{
    /// <summary>
    /// Methods can be called in any Thread, not thread safe
    /// </summary>
    public interface IFTPServerDataStore
    {
        /// <summary>
        /// Check if user is valid, note that the session ID is used as the user token after this.
        /// </summary>
        /// <param name="SessionID">A Unique identifier for this connection</param>
        /// <param name="UserName">The User's Username (coudld be anonomous)</param>
        /// <param name="Password">The User's Password</param>
        /// <returns>True if user is valid</returns>
        bool AuthenticateUser(Guid SessionID, String UserName, String Password);

        #region "  OpenFile  "
        Stream OpenFile(Guid SessionID, string Path);
        #endregion

        #region "  File/Directory Info  "
        DateTime GetModifiedTime(Guid SessionID, string Path);
        long GetSize(Guid SessionID, string Path);
        bool IsDirectory(Guid SessionID, string Path);
        Permissions GetPermissions(Guid SessionID, string Path);
        #endregion

        #region "  Get Directory Listings  "
        List<string> GetFiles(Guid SessionID, string Path);


        List<string> GetDirectories(Guid SessionID, string Path);
        #endregion

        #region Move, Delete, Create
        /// <summary>
        /// Move a Directory or File
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="OldPath"></param>
        /// <param name="NewPath"></param>
        /// <returns></returns>
        bool Move(Guid SessionID, string OldPath, string NewPath);

        /// <summary>
        /// Delete a Directory or File
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="Path"></param>
        /// <returns></returns>
        bool Delete(Guid SessionID, string Path);

        /// <summary>
        /// Create a New Directory
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="Path"></param>
        /// <returns></returns>
        bool CreateDirectory(Guid SessionID, string Path);
        #endregion
    }

    public class Permissions
    {
        //http://www.interspire.com/content/articles/12/1/FTP-and-Understanding-File-Permissions
        //todo:Add Set for Permission String

        public bool IsDirectory { get; set; }

        public bool OwnerRead { get; set; }
        public bool OwnerWrite { get; set; }
        public bool OwnerExecute { get; set; }
        public bool GroupRead { get; set; }
        public bool GroupWrite { get; set; }
        public bool GroupExecute { get; set; }
        public bool PublicRead { get; set; }
        public bool PublicWrite { get; set; }
        public bool PublicExecute { get; set; }

        public string PermissionString
        {
            get
            {
                string Return = "";
                if (IsDirectory)
                {
                    Return += "d";
                }
                else
                {
                    Return += "-";
                }
                if (OwnerRead)
                {
                    Return += "r";
                }
                else
                {
                    Return += "-";
                }
                if (OwnerWrite)
                {
                    Return += "w";
                }
                else
                {
                    Return += "-";
                }
                if (OwnerExecute)
                {
                    Return += "x";
                }
                else
                {
                    Return += "-";
                }

                if (GroupRead)
                {
                    Return += "r";
                }
                else
                {
                    Return += "-";
                }
                if (GroupWrite)
                {
                    Return += "w";
                }
                else
                {
                    Return += "-";
                }
                if (GroupExecute)
                {
                    Return += "x";
                }
                else
                {
                    Return += "-";
                }

                if (PublicRead)
                {
                    Return += "r";
                }
                else
                {
                    Return += "-";
                }
                if (PublicWrite)
                {
                    Return += "w";
                }
                else
                {
                    Return += "-";
                }
                if (PublicExecute)
                {
                    Return += "x";
                }
                else
                {
                    Return += "-";
                }

                return Return;
            }
        }

        public int OwnerPermissionNumber
        {
            get
            {
                int x = 0;
                if (OwnerRead)
                {
                    x += 4;
                }
                if (OwnerWrite)
                {
                    x += 2;
                }
                if (OwnerExecute)
                {
                    x += 1;
                }
                return x;
            }
            set
            {
                if (value>=4)
                {
                    OwnerRead = true;
                    value -= 4;

                }
                if (value>=2)
                {
                    OwnerWrite = true;
                    value -= 2;
                }
                if (value>=1)
                {
                    OwnerExecute = true;
                    value -= 1;
                }
            }
        }
        public int GroupPermissionNumber
        {
            get
            {
                int x = 0;
                if (GroupRead)
                {
                    x += 4;
                }
                if (GroupWrite)
                {
                    x += 2;
                }
                if (GroupExecute)
                {
                    x += 1;
                }
                return x;
            }
            set
            {
                if (value >= 4)
                {
                    GroupRead = true;
                    value -= 4;

                }
                if (value >= 2)
                {
                    GroupWrite = true;
                    value -= 2;
                }
                if (value >= 1)
                {
                    GroupExecute = true;
                    value -= 1;
                }
            }
        }
        public int PublicPermissionNumber
        {
            get
            {
                int x = 0;
                if (PublicRead)
                {
                    x += 4;
                }
                if (PublicWrite)
                {
                    x += 2;
                }
                if (PublicExecute)
                {
                    x += 1;
                }
                return x;
            }
            set
            {
                if (value >= 4)
                {
                    PublicRead = true;
                    value -= 4;

                }
                if (value >= 2)
                {
                    PublicWrite = true;
                    value -= 2;
                }
                if (value >= 1)
                {
                    PublicExecute = true;
                    value -= 1;
                }
            }
        }
    }
}
