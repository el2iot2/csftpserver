﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csftpserver.Replies
{
    //http://www.networksorcery.com/enp/protocol/ftp.htm
    interface IReply
    {
        public string ReplyText { get; }
        public int Code { get; }
    }
}
