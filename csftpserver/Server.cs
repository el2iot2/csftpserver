﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csftpserver
{
    public static class Server
    {

        #region "  Private  "
        static Server()
        {
            BW.WorkerReportsProgress = true;
            BW.WorkerSupportsCancellation = true;
            BW.DoWork += new System.ComponentModel.DoWorkEventHandler(BW_DoWork);
            BW.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(BW_ProgressChanged);
        }
        
        static void BW_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (Log != null)
            {
                Log(DataStore, new LogEntry() { LogCode = e.ProgressPercentage, LogText = e.UserState.ToString() });
            }
        }

        static void BW_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            throw new NotImplementedException();
        }

        
        private static int PortNumber;
        #endregion

        #region "  Internal  "
        /// <summary>
        /// Calling the ReportProgress method allows you to add to the log, in a thread safe manor
        /// </summary>
        internal static System.ComponentModel.BackgroundWorker BW = new System.ComponentModel.BackgroundWorker();

        internal static IFTPServerDataStore DataStore;
        #endregion

        #region "  Public  "
        public static event EventHandler<LogEntry> Log;
        public static void Initialize(IFTPServerDataStore DataStore)
        {
            Server.DataStore = DataStore;
        }
        public static void Initialize(IFTPServerDataStore DataStore, int PortNumber)
        {
            Server.PortNumber = PortNumber;
            Initialize(DataStore);
        }

        public static void StopServer()
        {
        }

        public List<Session> Sessions = new List<Session>();
        #endregion
    }

    public class LogEntry:EventArgs
    {
        public int LogCode { get; set; }
        public string LogText { get; set; }
    }
}
